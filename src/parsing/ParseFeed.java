package parsing;


import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.geo.Location;
import processing.core.PApplet;
import processing.data.XML;

import java.util.ArrayList;
import java.util.List;

public class ParseFeed {

	public static List<PointFeature> parseEarthquake(PApplet p, String fileName) {
		List<PointFeature> features = new ArrayList<>();
		XML rss = p.loadXML(fileName);
		XML[] itemXML = rss.getChildren("entry");
		PointFeature point;
		for (XML anItemXML : itemXML) {
			Location location = getLocationFromPoint(anItemXML);
			if (location != null) {
				point = new PointFeature(location);
				features.add(point);
			} else {
				continue;
			}
			String titleStr = getStringVal(anItemXML, "title");
			if (titleStr != null) {
				point.putProperty("title", titleStr);
				point.putProperty("magnitude", Float.parseFloat(titleStr.substring(2, 5)));
			}
			float depthVal = getFloatVal(anItemXML, "georss:elev");
			int interVal = (int) (depthVal / 100);
			depthVal = (float) interVal / 10;
			point.putProperty("depth", Math.abs((depthVal)));
			XML[] catXML = anItemXML.getChildren("category");
			for (XML aCatXML : catXML) {
				String label = aCatXML.getString("label");
				if ("Age".equals(label)) {
					String ageStr = aCatXML.getString("term");
					point.putProperty("age", ageStr);
				}
			}
		}
			return features;
		}

	private static Location getLocationFromPoint(XML itemXML) {
		Location loc = null;
		XML pointXML = itemXML.getChild("georss:point");
		if (pointXML != null && pointXML.getContent() != null) {
			String pointStr = pointXML.getContent();
			String[] latLon = pointStr.split(" ");
			float lat = Float.valueOf(latLon[0]);
			float lon = Float.valueOf(latLon[1]);
			loc = new Location(lat, lon);
		}
		return loc;
	}	
	
	private static String getStringVal(XML itemXML, String tagName) {
		String str = null;
		XML strXML = itemXML.getChild(tagName);
		if (strXML != null && strXML.getContent() != null) {
			str = strXML.getContent();
		}
		return str;
	}
	
	private static float getFloatVal(XML itemXML, String tagName) {
		return Float.parseFloat(getStringVal(itemXML, tagName));
	}
}