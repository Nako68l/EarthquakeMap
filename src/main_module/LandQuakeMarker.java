package main_module;

import de.fhpotsdam.unfolding.data.PointFeature;
import processing.core.PGraphics;

public class LandQuakeMarker extends EarthquakeMarker {

    LandQuakeMarker(PointFeature quake) {
        super(quake);
        isOnLand = true;
    }

    @Override
    public void drawEarthquake(PGraphics pg, float x, float y) { pg.rect(x-buffer, y-buffer, radius, radius); }

    void drawX(PGraphics pg, float x, float y){
        pg.line(x-buffer,
                y-buffer,
                x-buffer+radius,
                y-buffer+radius);
        pg.line(x-buffer,
                y-buffer+radius,
                x-buffer+radius,
                (y-buffer));
    }
}
