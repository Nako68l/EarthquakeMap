package main_module;

import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;
import processing.core.PConstants;
import processing.core.PGraphics;


public abstract class EarthquakeMarker extends SimplePointMarker{
    boolean isOnLand;
    float radius;
    float buffer = radius/2;

    private static final float LIGHT_DEPTH = 10;
    private static final float INTERMEDIATE_DEPTH = 70;
    private static final float BIG_DEPTH = 250;

    public abstract void drawEarthquake(PGraphics pg, float x, float y);

    public void draw(PGraphics pg, float x, float y) {
        if (selected) {
            showTitle(pg, x, y);
        }
        drawMarker(pg, x, y);
    }

    EarthquakeMarker(PointFeature feature)
    {
        super(feature.getLocation());
        java.util.HashMap<String, Object> properties = feature.getProperties();
        float magnitude = Float.parseFloat(properties.get("magnitude").toString());
        properties.put("radius", 2*magnitude );
        setProperties(properties);
        this.radius = 2.5f*getMagnitude();
    }

    private void drawMarker(PGraphics pg, float x, float y) {
        pg.pushStyle();

        colorDetermine(pg);
        drawEarthquake(pg, x, y);

        String age = getStringProperty("age");
        if ("Past Hour".equals(age) || "Past Day".equals(age)) {
            pg.strokeWeight(2);
           drawX(pg,x,y);
        }
        pg.popStyle();
    }

    void drawX(PGraphics pg, float x, float y){}

    private void showTitle(PGraphics pg, float x, float y)
    {
        String title = getTitle();
        pg.pushStyle();

        pg.stroke(110);
        pg.fill(255,210,177);
        pg.rect(x, y + 15, pg.textWidth(title) +6, 18, 5);

        pg.textAlign(PConstants.LEFT, PConstants.TOP);
        pg.fill(0);
        pg.text(title, x + 3 , y +18);


        pg.popStyle();
    }

    private void colorDetermine(PGraphics pg) {
        float depth = getDepth();
        if (depth<LIGHT_DEPTH)pg.fill(255,243,178);
        else if (depth<INTERMEDIATE_DEPTH)pg.fill(255,239,0);
        else if (depth<BIG_DEPTH)pg.fill(255,159,50);
        else pg.fill(255,0,0);
    }

    private float getMagnitude() { return Float.parseFloat(getProperty("magnitude").toString());    }

    private float getDepth() { return Float.parseFloat(getProperty("depth").toString()); }

    private String getTitle() { return (String) getProperty("title"); }
}
