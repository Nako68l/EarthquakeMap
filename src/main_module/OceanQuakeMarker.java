package main_module;

import de.fhpotsdam.unfolding.data.PointFeature;
import processing.core.PGraphics;
public class OceanQuakeMarker extends EarthquakeMarker {
	
	OceanQuakeMarker(PointFeature quake) {
		super(quake);
		isOnLand = false;
	}
	public void drawEarthquake(PGraphics pg, float x, float y) { pg.ellipse(x, y, radius, radius); }


	@Override
	void drawX(PGraphics pg, float x, float y) {
		pg.line(x-buffer,
				y-buffer,
				x+buffer,
				y+buffer);
		pg.line(x-buffer,
				y+buffer,
				x+buffer,
				y-buffer);
	}
}
