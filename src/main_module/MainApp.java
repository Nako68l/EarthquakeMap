package main_module;

import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.data.Feature;
import de.fhpotsdam.unfolding.data.GeoJSONReader;
import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.AbstractShapeMarker;
import de.fhpotsdam.unfolding.marker.Marker;
import de.fhpotsdam.unfolding.marker.MultiMarker;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;
import de.fhpotsdam.unfolding.providers.Google;
import de.fhpotsdam.unfolding.providers.Microsoft;
import de.fhpotsdam.unfolding.utils.MapUtils;
import parsing.ParseFeed;
import processing.core.PApplet;

import java.util.ArrayList;
import java.util.List;

public class MainApp extends PApplet {
    private UnfoldingMap map;

    private String earthquakesURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.atom";
    private String cityFile = "data/city-data.json";
    private String countryFile = "data/countries.geo.json";

    private SimplePointMarker lastSelected;

    private List<Marker> countryMarkers;
    private List<Marker> cityMarkers;
    private ArrayList<Marker> quakeMarkers;
    private List<PointFeature> earthquakes = ParseFeed.parseEarthquake(this, earthquakesURL);

    public void setup() {
        size(985, 700, OPENGL);
        map = new UnfoldingMap(this, 200, 0, 800, 700, new Google.GoogleMapProvider());
        MapUtils.createDefaultEventDispatcher(this, map);

        List<Feature> countries = GeoJSONReader.loadData(this, countryFile);
        //noinspection ConstantConditions
        countryMarkers = MapUtils.createSimpleMarkers(countries);

        List<Feature> cities = GeoJSONReader.loadData(this, cityFile);
        cityMarkers = new ArrayList<>();
        //noinspection ConstantConditions
        for (Feature city : cities) {
            cityMarkers.add(new CityMarker(city));
        }

        quakeMarkers = new ArrayList<>();
        for (PointFeature feature : earthquakes) {
            if (isLand(feature)) {
                quakeMarkers.add(new LandQuakeMarker(feature));
            }
            else {
                quakeMarkers.add(new OceanQuakeMarker(feature));
            }
        }

        printQuakes();
        map.addMarkers(quakeMarkers);
        map.addMarkers(cityMarkers);
    }

    private boolean isLand(PointFeature earthquake) {
        for (Marker country : countryMarkers) {
            if (isInCountry(earthquake, country)) {
                return true;
            }
        }
        return false;
    }

    private void printQuakes() {
        System.out.println("Number of earthquakes for past week:");
        for (int i = 0; i < countryMarkers.size(); i++) {
            int eqCount = 0;
            int inOcean = 0;

            for (PointFeature earthquake : earthquakes) {
                if (countryMarkers.get(i).getProperty("name") == (earthquake.getProperty("country"))) eqCount++;
                if (i > 0 && (i == countryMarkers.size() - 1) && (earthquake.getProperty("country") == null))
                    inOcean++;

            }
            if (eqCount > 0) System.out.println(countryMarkers.get(i).getProperty("name") + " " + eqCount);
            if (inOcean > 0) System.out.println("Ocean " + inOcean);
        }
    }

    private boolean isInCountry(PointFeature earthquake, Marker country) {
        Location checkLoc = earthquake.getLocation();

        if (country.getClass() == MultiMarker.class) {
            for (Marker marker : ((MultiMarker) country).getMarkers()) {
                if (((AbstractShapeMarker) marker).isInsideByLocation(checkLoc)) {
                    earthquake.addProperty("country", country.getProperty("name"));
                    return true;
                }
            }
        } else if (((AbstractShapeMarker) country).isInsideByLocation(checkLoc)) {
            earthquake.addProperty("country", country.getProperty("name"));
            return true;
        }
        return false;
    }

    public void mouseMoved()
    {
        if (lastSelected != null) {
            lastSelected.setSelected(false);
            lastSelected = null;

        }
        selectMarkerIfHover(quakeMarkers);
        selectMarkerIfHover(cityMarkers);
    }

    private void selectMarkerIfHover(List<Marker> markers)
    {
        if (lastSelected != null) {
            return;
        }
        for (Marker m : markers)
        {
            SimplePointMarker marker = (SimplePointMarker) m;
            if (marker.isInside(map,  mouseX, mouseY)) {
                lastSelected = marker;
                marker.setSelected(true);
                return;
            }
        }
    }

    private void addKey() {
        fill(255, 250, 240);

        int xbase = 15;
        int ybase = 15;

        rect(xbase, ybase, 185, 320);

        fill(0);
        textSize(19);
        text("Earthquake Map", xbase + 18   , ybase + 25);
        textSize(15);
        text("Size ~ Magnitude", xbase + 25, ybase + 150);
        textSize(13);
        fill(150, 30, 30);

        int tri_xbase = xbase + 25;
        int tri_ybase = ybase + 50;
        int triSize = CityMarker.TRI_SIZE+4;
        triangle(tri_xbase, tri_ybase -triSize , tri_xbase - triSize,
                tri_ybase + triSize, tri_xbase + triSize,
                tri_ybase + triSize);

        fill(0, 0, 0);
        text("City Marker", xbase + 45, tri_ybase+5);
        text("Ocean Quake", xbase + 45, ybase + 85);
        text("Land Quake", xbase + 45, ybase + 115);

        fill(255, 255, 255);
        ellipse(xbase + 25,ybase + 80,20,20);
        rect(xbase + 16, ybase + 100, 20, 20);

        noStroke();
        fill(255,243,178);
        rect(xbase + 20, ybase + 170, 20, 20, 7);
        fill(255,239,0);
        rect(xbase + 20, ybase + 195, 20, 20, 7);
        fill(255,159,50);
        rect(xbase + 20, ybase + 220, 20, 20, 7);
        fill(255,0,0);
        rect(xbase + 20, ybase + 245, 20, 20, 7);

        fill(0,0,0);
        text("Light", xbase+50,ybase+185);
        text("Shallow", xbase+50,ybase+210);
        text("Intermediate", xbase+50,ybase+235);
        text("Deep", xbase+50,ybase+260);
        text("Past day",xbase+50,ybase+290);

        stroke(0,0,0);
        fill(255, 255, 255);
        int centerx = xbase + 30;
        int centery = ybase + 285;
        ellipse(centerx, centery, 17, 17);

        strokeWeight(2);
        line(centerx - 10, centery - 10, centerx + 10, centery + 10);
        line(centerx - 10, centery + 10, centerx + 10, centery - 10);
    }

    public void draw() {
        background(214,255,183);
        map.draw();
        addKey();
    }

    public static void main(String[] args) {
        PApplet.main(MainApp.class.getName());
    }
}
