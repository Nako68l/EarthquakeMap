package addition;

import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.data.Feature;
import de.fhpotsdam.unfolding.data.GeoJSONReader;
import de.fhpotsdam.unfolding.marker.Marker;
import de.fhpotsdam.unfolding.providers.Microsoft;
import de.fhpotsdam.unfolding.utils.MapUtils;
import processing.core.PApplet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LifeExpectancy extends PApplet {
    private UnfoldingMap map;
    private Map<String, Float> lifeExpByCountry;
    private List<Marker> countryMarker;

    private int light_yellow = color(247,255,208);
    private int light_blue = color(182, 229, 255);
    private int black = color (0, 0, 0);

    private Map<String, Float> loadLifeExpFromCVS(String fileName){
        Map<String, Float> lifeExpMap = new HashMap<>();
        String[] rows = loadStrings(fileName);
        for (String row: rows) {
            String[] column = row.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            if (column.length == 5 && column[4].matches("(([-+])?[0-9]+(\\.[0-9]+)?)+")){
                float value = Float.parseFloat(column[4]);
                lifeExpMap.put(column[3], value);
            }
        }
        return lifeExpMap;
    }

    private void shadeCountries(){
            for (Marker marker: countryMarker){
                String countryId = marker.getId();

                if (lifeExpByCountry.containsKey(countryId)){
                    float lifeExp = lifeExpByCountry.get(countryId);
                    int colorLevel = (int) map(lifeExp, 40, 90, 10, 255);
                    marker.setColor(color(255-colorLevel, 100, colorLevel));
                } else {
                    marker.setColor(color(150,150,150));
                }
            }

    }
    public void setup(){
        size(1000, 700, OPENGL);
        map = new UnfoldingMap(this, 220, 0, 800, 700, new Microsoft.AerialProvider());
        MapUtils.createDefaultEventDispatcher(this,map);

        lifeExpByCountry = loadLifeExpFromCVS("data/life-expectancy.csv");

        List<Feature> countries = GeoJSONReader.loadData(this, "data/countries.geo.json");
        //noinspection ConstantConditions
        countryMarker = MapUtils.createSimpleMarkers(countries);

        map.addMarkers(countryMarker);
        shadeCountries();
    }
    public void draw(){
        background(light_blue);
        map.draw();
        addKey();
    }
    private void addKey(){
        fill(light_yellow);
        rect(20,20,180,210);
        fill(black);
        textSize(18);
        text("Life expectancy",43,50);
        textSize(13);
        fill(39,100,216);
        rect(30, 70, 18, 18);
        fill(black);
        text("74+ years",60,85);
        fill(94,100,161);
        rect(30, 105, 18 ,18);
        fill(black);
        text("68 - 74 years", 60,120);
        fill(190,100,65);
        rect(30, 140, 18 ,18);
        fill(black);
        text("<68 years", 60,155);
        fill(150,150,150);
        rect(30, 175, 18 ,18);
        fill(black);
        text("Unknown", 60,190);
    }

    public static void main(String[] args) {
        PApplet.main(LifeExpectancy.class.getName());
    }
}
